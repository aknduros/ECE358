
from socket import *
import os
from datetime import datetime
import struct
import random
##### HEADER FIELDS REMAINING CONSTANT FOR THE PURPOSES OF THIS LAB AS SPECIFIED IN THE MANUAL
message = 0
HEADER_ID = 0 # Random generated 16b TODO
### FLAGS SECTION: ###
# *HEADER_QR: (1 bit) if query 0b, if response 1b TODO
#HEADER_OPCODE = 0000 # not sure if this is correct for python - but its supposed to be 4 bits long and set as 0
# *HEADER_AA: TODO (1 bit) manual says set to 1 for this lab but piazza TA says its more correct for it to be 0 for querys and 1 for responses
#HEADER_TC = 0 # should be 1 bit long
#HEADER_RD = 0 # should be 1 bit long
#HEADER_RA = 0 # should be 1 bit long
#HEADER_Z = 000 # should be 3 bits long
#HEADER_RCODE = 0000 # should be 4 bits long
# done with flags section
HEADER_QDCOUNT = 1 # unsigned 16 bit integer - set to 1
HEADER_ANCOUNT = 0 # TODO unsigned 16 bit integer -- default 0 - the response SHOULD BE DIFFERENT
HEADER_NSCOUNT = 0 # unsigned 16 bit integer - set to 0
HEADER_ARCOUNT = 0 # unsigned 16 bit integer - set to 0

##### QUESTION SECTION FIELDS CONSTANT FOR THE PURPOSES OF THIS LAB AS SPECIFIED IN THE MANUAL
QTYPE = 1  # should be 2 octets (16 bits) and for TYPE A, the value is 1 (should display 00 01 in hex)
QCLASS = 1 # should be 2 octets (16 bits) and for class IN, the value is 1 (should display 00 01 in hex) for the internet

##### ANSWER SECTION FIELDS CONSTANT FOR THE PURPOSES OF THIS LAB AS SPECIFIED IN THE MANUAL -- should probably keep in server instead but also want to initialize the ttl and rdata/rlength stuff to extract later idk
# TODO figure out if should be kept here and what should be in server
ANS_NAME = 0xc00c # not sure if thats the right formatting (TODO) - the field realisitcally will vary but here it will always be c0 0c in hex.
ANS_TYPE = QTYPE # TODO: once figured out Qtype formatting, use the same here instead of copying over the variable 
ANS_CLASS = QCLASS # TODO: once figured out Qclass, use the same here instead of copying over variable
# *ANS_TTL: 32 bit unsigned int set based on domain table (260: 00 00 01 04 in hex; 160: 00 00 00 a0 in hex; if domain missing or error: 00 00 00 00)
# *ANS_RDLENGTH: 1 bit uint TODO
# *ANS_RDATA: variable length string of octets (IP addresses!! e.g. 192.165.1.XX is c0 a5 01 and then last is what is changing - for google, print both but the second should loop to the start of the answer section, so include everything from c0 0c to here again)


clientInput = "google.com"

# header production - TODO figure out how to assemble the bit lengths and also on the server side we need to be able to modify QR, AA, and ANCOUNT so those should be extractable
HEADER_ID = random.getrandbits(16) #16 bit random sequence, shouldn't change btwn here and response
FLAGS = 0 #Only changing on the response side! (flields QR and AA changing, so integer value is either 0 or 33792)
HEADER_ANCOUNT = 0 # 0 for query, 1 for response
    # Creating DNS Header
message=struct.pack('>HHHHHH',HEADER_ID,FLAGS,HEADER_QDCOUNT,HEADER_ANCOUNT,HEADER_NSCOUNT,HEADER_ARCOUNT)
## TESTING
#print(message.hex(' '))

# producing the qname field for the query
query_qname = ""
# For each section of the user input separated by a period
for input in clientInput.lower().split('.'):
  # Append the ASCII character with the code length len(input)
  query_qname += chr(len(input))
  query_qname += input
  # at the Termination of the loop add 00
query_qname += chr(0)
  ## TESTING -- this print is to make sure that the qname that will be printed from the server is accurate
#print(query_qname.encode().hex(' '))

  # Adding on the Query Section to the DNS Header
message += query_qname.encode()
message += struct.pack('>HH',QTYPE,QCLASS)
  ## TESTING
print(message[12:-4].decode('UTF-8'))
print(message[12:-3])


DomainTable = {
                'googlecom':(260,["192.165.1.1","192.165.1.10"]),     # ("googlecom",'A',"IN"):("260","192.165.1.1","192.165.1.10"),
                "youtubecom":(160,"192.165.1.2"),                   # ("youtube.com",'A',"IN"):(160,"192.165.1.2"),
                "uwaterlooca":(160,"192.165.1.3"),                  # ("uwaterloo.ca",'A',"IN"):("160","192.165.1.3"),
                "wikipediaorg":(160,"192.165.1.4"),                 # ("wikipedia.org",'A',"IN"):("160","192.165.1.4"),
                "amazonca":(160,"192.165.1.5")                      # ("amazon.ca",'A',"IN"):("160","192.165.1.5")
            }
z = str(message[12:-4].decode('UTF-8'))
print(z[1])
size = int.from_bytes(message[12:13], "big")
inital = size
subi = 0
domain = ""
for i in range(len(message[12:-6])):
    if(size > 0):
        domain += message[13+i:14+i].decode()
        size -= 1
        subi += 1
    elif(subi == inital):
        if(size == 0):
            if(int.from_bytes(message[13+i:14+i], "big") == 0):
                break
        size = int.from_bytes(message[13+i:14+i], "big")
        inital = size
        subi = 0
print(domain.encode())
IPs = ["192.165.1.10","192.165.1.1"]
ENCODEDIP = []


if(len(IPs) > 1):
    for ip in IPs:
        sections = ip.split('.')
        ipints = []
        for byte in sections:
            ipints.append(int(byte))
        ENCODEDIP.append(struct.pack('>BBBB', ipints[0], ipints[1], ipints[2], ipints[3]))
    pass
print(struct.pack('>H', 49164))
        