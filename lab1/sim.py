import math
import random
import heapq 

# K or the desired buffer length will provided through a command line prompt (-1 is infinite)
# T or the desired simulation time (seconds) will be provided through a command line prompt

# The transmission rate of the output link in bits per second (1Mbps)
C = 1000000
# Average length of a packet in bits. will follow an exponential distribution (2000bits)
L = 2000

# Global variables 
arrival_t = 0
observe_t = 0
departure_t = 0

# DES Queue for all events implimented with heapq
all_events = []

## Simulation exp rand Var
def generate_rv(lamb):
    u = random.uniform(0, 1)
    x = -(1/lamb) * math.log(1-u)
    return x

# 3 types of events (Arrival, Departure, Observer)
def generate_arrival(rho): 
    global arrival_t
    
    # this is 𝝀 Average number of packets generated /arrived (packets per second)
    lambd = (rho * C)/L 
    # will be generated via random variable and that is added to the current timestamp based on the previous arival 
    arrival_t += generate_rv(lambd)
    arrive = (arrival_t,"Arrival")
    heapq.heappush(all_events, arrive)
    # since the packet successfully arrived, the departure event can now be created
    generate_departure()
    
    return

def generate_observation(rho):
    global observe_t
    
    # Observation rate needs to be 5 times as much as the arrival rate
    obsv_rate = ((rho * C)/L) * 5
    observe_t +=  generate_rv(obsv_rate)

    observe = (observe_t,"Observe")
    # add to queue of events
    heapq.heappush(all_events, observe)

    return


def generate_departure():
    global departure_t,arrival_t
    
    serv_t = (generate_rv(1/L))/C
    
    if (arrival_t > departure_t):
        # first arrival
        departure_t = arrival_t + serv_t
    else:
        departure_t += serv_t
    
    depart = (departure_t, "Departure")
    heapq.heappush(all_events, depart)
    
    return


def simulate(rho, K):
    global arrival_t, observe_t, departure_t
    
    #SIMULATION VARIABLES
    En = 0                  # Average number of packets in the buffer/queue
    
    arrivals = 0            # Counter for the arrival packets
    generated = 0           # Counter for the total generated packets
    departures = 0          # Counter for the departure packets
    
    arrival_t = 0           # Arrival time
    observe_t = 0           # Observation time
    departure_t = 0         # Departure time

    # Storing the number of packets at various observation times
    packets_check = []
    # Traffic intensity
    arrival_rate = (rho * C)/L

    if(K == -1): # INFINITE BUFFER CASE MM1

        # Infinite buffer case only metric variables:
        prob_idle = 0       # The proportion of time the server is idle
        observations = 0    # Number of observations
        q_idle = 0          # times the queue was observed to be idle

        # Generation section for MM1
        while(arrival_t < T):
            generate_arrival(rho)
        while(observe_t < T):
            generate_observation(rho)
        
        # Simulation section for MM1
        while(len(all_events) > 0):
            ev = heapq.heappop(all_events)
            if(ev[1] == "Arrival"):
                arrivals += 1
            elif(ev[1] == "Departure"):
                departures += 1
            elif(ev[1] == "Observe"):
                q_status = arrivals - departures
                packets_check.append(q_status)
                observations += 1
                if(arrivals == departures): q_idle += 1
        
        # Calculate the Expectation (avg packets) and the P idle metrics & return
        prob_idle = q_idle/observations
        En = sum(packets_check)/len(packets_check)
        
        return En, prob_idle
    
    
    else: # FINITE BUFFER CASE MM1K
        
        # Finite buffer case only metric variables:
        prob_loss = 0       # The packet loss probability (for M/M/1/K queue). 
        p_in_q = 0          # Number of packets currently in queue
        p_lost = 0          # Number of lost packets

        # Generation section for MM1K
        while(arrival_t < T):
            arrival_t += generate_rv(arrival_rate)
            arrive = (arrival_t,"Arrival")
            # Counting the amount of generated packets as they are produced
            generated += 1
            heapq.heappush(all_events, arrive)
        while(observe_t < T):
            generate_observation(rho)
        
        # Simulation section for MM1K
        while(len(all_events) > 0):
            # ev is the event being processed (0: timestamp, 1: type)
            ev = heapq.heappop(all_events)
            if(ev[1] == "Arrival"):
                if(p_in_q < K): # Has not reached the maximum amount of packets for the queue
                    arrivals += 1
                    p_in_q += 1
                    
                    # Schedule Departure 
                    serv_t = (generate_rv(1/L))/C
                    # in condition, ev[0] should hold the arrival time of the event
                    if (ev[0] > departure_t):
                        # first arrival
                        departure_t = ev[0] + serv_t 
                    else:
                        departure_t += serv_t
                    # Create the Departure Event once processed
                    heapq.heappush(all_events, (departure_t, "Departure"))
                else:
                    # The buffer is full and now the amount of packets lost increases
                    p_lost += 1 
            elif(ev[1] == "Departure"):
                p_in_q -= 1
                departures += 1
            elif(ev[1] == "Observe"):
                q_status = arrivals - departures
                packets_check.append(q_status)
        
        # Calculate the Expectation (avg packets) and the P loss metrics & return
        En = sum(packets_check)/len(packets_check)
        prob_loss = p_lost/generated

        return En, prob_loss   

#main like c++
if __name__ == "__main__":
    global T
    
    q = int(input("Which question are you simulating? (1, 3, 4, or 6): "))
    if (q==1) :
        # Question 1
        rv_array = []
        variance = 0
        for i in range(0,1000):
            rv_array.append(generate_rv(75))
        mean = sum(rv_array)/1000
        for x in rv_array:
            variance += (x - mean)**2   
        variance = variance/1000 
        print("Mean: " + str(mean) + "Var: " + str(variance))
    else:
        # For Questions 3,4, or 6.
        # T is taken in for all options, and unless specified, rho is default 1.2 (question 4)
        T = int(input("Enter the value of T (seconds): "))
        rho = 1.2
        if q==3:
            # Question 3, Simulating MM1 (infinite) Queue over an intensity range
            cont = bool(input("Question 3 (M/M/1 queue) has parameters: L=2000, C=1Mbps, 0.25< Rho <0.95 (step size 0.1). Would you like to proceed? (1 yes/0 no): "))
            # for infinite buffer, K is set as -1
            K = -1
            if cont:
                # Default starting rho intensity:
                rho = 0.25
                
                with open('output_q3.csv', 'w') as f:
                    f.write("Rho,E[n],P_Idle"+ '\n')
                while (rho < 0.95):
                    q3_exp, q3_idle = simulate(rho, K)
                    with open('output_q3.csv', 'a') as f:
                        f.write(str(rho) + ',' + str(q3_exp) + ',' + str(q3_idle) +  '\n')
                    # stepping rho by 0.1 as outlined in the question
                    rho += 0.1
        elif q==4:
            # Question 4, Simulating MM1 Queue with rho = 1.2
            cont = bool(input("Question 4 (M/M/1 queue) has parameters: L=2000, C=1Mbps, Rho = 1.2. Would you like to proceed? (1 yes/0 no): "))
            K = -1
            rho = 1.2
            if cont:
                q4_exp, q4_idle = simulate(rho, K)
                print("E[n]: " + str(q4_exp) + "; P_idle: " + str(q4_idle))
        elif q==6:
            # Question 6, Simulating MM1K (finite) Queue over an intensity range and over 3 buffer sizes (K)
            cont = bool(input("Question 6 (M/M/1/K queue) has parameters: L=2000, C=1Mbps, 0.50< Rho <1.5 (step size 0.1). Would you like to proceed? (1 yes/0 no): "))
            if cont:
                with open('output_q6.csv', 'w') as f:
                    f.write("K,Rho,E[n],P_Loss"+ '\n')
                for x in range(0,3):
                    K = int(input("Enter the value of K: "))
                    rho = 0.5
                    while (rho < 1.5):
                        q6_exp, q6_loss = simulate(rho, K)
                        with open('output_q6.csv', 'a') as f:
                            # Outputting the K, rho, Expectation, and P loss to csv
                            f.write(str(K) + ',' + str(rho) + ',' + str(q6_exp) + ',' + str(q6_loss) + '\n')
                        # stepping rho by 0.1 as outlined in the question
                        rho += 0.1
                    with open('output_q6.csv', 'a') as f:
                        f.write('\n')
    
    pass