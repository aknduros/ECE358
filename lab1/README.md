# ECE 358 Lab 1 Packet Queue Simulation 

sim.py is a simulator for M/M/1 and an M/M/1/K packet queue. The readme describes:

### - How to make/run sim.py
### - How to respond to comand line promts 
### - How to clean generated csv files 

## How to make/run sim.py
To run sim.py use the Makefile
```bash
  make
```

## How to respond to comand line promts 
Once you run the python script you  will be prompted to enter the lab question you would like to simulate for (Which question are you simulating? 
 
Enter enter a number and continue onto other prompts

The data generated from your simulation will be written into .csv files in the local directory 
#### note: csv's will be overwitten each time you run sim.py

## How to clean generated csv files 
Run this remove all .csv files from the local directory 
```bash
  make clean
```

## Authors: 
### Group 15
- Albert Ndur-Osei
- Emilia Zerbe
