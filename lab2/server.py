from socket import *
import struct

DomainTable = {
                "googlecom":(260,["192.165.1.1","192.165.1.10"]),    #("googlecom",'A',"IN"):("260","192.165.1.1","192.165.1.10"),
                "youtubecom":(160,["192.165.1.2"]),                  #("youtube.com",'A',"IN"):(160,"192.165.1.2"),
                "uwaterlooca":(160,["192.165.1.3"]),                 #("uwaterloo.ca",'A',"IN"):("160","192.165.1.3"),
                "wikipediaorg":(160,["192.165.1.4"]),                #("wikipedia.org",'A',"IN"):("160","192.165.1.4"),
                "amazonca":(160,["192.165.1.5"])                     #("amazon.ca",'A',"IN"):("160","192.165.1.5")
            }

serverIP = "127.0.0.1"
serverPort = 13000
serverSocket = socket(AF_INET, SOCK_DGRAM)
serverSocket.bind((serverIP, serverPort))
print ("The server is ready to receive")
while True:
    dnsQuery, clientAddress = serverSocket.recvfrom(2048)
    ## print CLIENT dnsQuery as hex
    print("Request: ")
    print(dnsQuery.hex(' '))

    # Parse client DNS query 
    packedHeader = dnsQuery[:12] # Header length is always going to be 12
    HEADER_ID,FLAGS,HEADER_QDCOUNT,HEADER_ANCOUNT,HEADER_NSCOUNT,HEADER_ARCOUNT = struct.unpack('>HHHHHH', packedHeader)
    
    #get domain 
    QUESTION = dnsQuery[12:] # Query section of what was sent
    size = int.from_bytes(dnsQuery[12:13], "big")
    inital = size
    subi = 0
    domain = ""
    for i in range(len(dnsQuery[12:-6])):
        if(size > 0):
            domain += dnsQuery[13+i:14+i].decode()
            size -= 1
            subi += 1
        elif(subi == inital):
            if(size == 0):
                if(int.from_bytes(dnsQuery[13+i:14+i], "big") == 0):
                    break
            size = int.from_bytes(dnsQuery[13+i:14+i], "big")
            inital = size
            subi = 0
            
    if domain in DomainTable:
        # the domain exists!
        FLAGS = 33792 # changing on the response side! (flields QR and AA changing, so integer value is either 0 or 33792)
        HEADER_ANCOUNT = 1 # 0 for query, n for response n = number of records 
        ANS_NAME = 49164 # will always be c0 0c in hex.
        ANS_TTL, IPs = DomainTable[domain] # 32 bit UNsigned int (in this lab know 260: 00 00 01 04 in hex; 160: 00 00 00 a0 in hex)
        ANS_TYPE = 1 # in hex 0001
        ANS_CLASS = 1 
        ENCODEEDIPs = []
        ANS_RDLENGTH = 0 # in hex 0004 later (this lab 4 always)
        
        # wont enter if no IPs in domain table:
        if(len(IPs) > 0):
            HEADER_ANCOUNT = 0 # 0 to start, will incement for each IP record found
            for ip in IPs: # for each IP (ip here is entire record, not parsed yet) found
                HEADER_ANCOUNT += 1
                sections = ip.split('.')
                ANS_RDLENGTH = len(sections) # lab expects will be 4, but covers if not 4 sections, later it is 4 by hardcode
                ipints = []
                for byte in sections:
                    ipints.append(int(byte))
                ENCODEEDIPs.append(struct.pack('>BBBB', ipints[0], ipints[1], ipints[2], ipints[3])) # different sections
            # Constructing the return message
            ANS_HEAD = struct.pack('>HHHHHH', HEADER_ID,FLAGS,HEADER_QDCOUNT,HEADER_ANCOUNT,HEADER_NSCOUNT,HEADER_ARCOUNT)
            ANS_MSG = ANS_HEAD + QUESTION
            for ip in ENCODEEDIPs: # here ip is the digit sections without their periods
                ANS_MSG += struct.pack('>HHHIH', ANS_NAME, ANS_TYPE, ANS_CLASS, ANS_TTL, ANS_RDLENGTH) # TTL: UNSIGNED int (I)
                ANS_MSG += ip
            
            ## print CLIENT dnsQuery AND the RESPONSE as hex
            print("Response: ")
            print(ANS_MSG.hex(' ')) # print response as hex in the terminal
            
            # Send back the compiled sections to the client
            serverSocket.sendto(ANS_MSG,clientAddress)
            
    else:
        # FOR ERROR HANDLING (if domain not existing or is no records for a domain that might exist): 
        ### if www but still correct - manual says its an error so leave it!!

        # In the header, even if no domain existing or no record, the QR bit in FLAG still changes to 1
        FLAGS = 33792 # changing on the response side! (flields QR and AA changing, so integer value is either 0 or 33792)
        ANS_HEAD = struct.pack('>HHHHHH', HEADER_ID,FLAGS,HEADER_QDCOUNT,HEADER_ANCOUNT,HEADER_NSCOUNT,HEADER_ARCOUNT)
        # Remaining info that was originally sent is stored in the variable QUESTION

        # the rest of header and question is the same. HEADER_ANCOUNT still being 0 will flag client that theres an error.
        ANS_NAME = 49164 # still hex c0 0c and the rest of the fields will mean nothing
        ANS_TYPE = 0
        ANS_CLASS = 0
        ANS_TTL = 0 # zero for volatile data / ones that should not be cached
        ANS_RDLENGTH = 0
        RDATA_ERR = 0 # could fill with dummy ips but just set as zero and then pack it as 2 octets worth
        ANS_MSG = ANS_HEAD + QUESTION
        ANS_MSG +=  struct.pack('>HHHIHH', ANS_NAME, ANS_TYPE, ANS_CLASS, ANS_TTL, ANS_RDLENGTH, RDATA_ERR)

        # print CLIENT dnsQuery AND the RESPONSE as hex (ERROR)
        print("Response: ")
        print(ANS_MSG.hex(' ')) # print response as hex in the terminal
        
        # Send back the compiled sections to the client
        serverSocket.sendto(ANS_MSG,clientAddress)