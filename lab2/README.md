# ECE 358 Lab 2 Socket Programming using Python 

webserver.py is a web server that handles one HTTP request (GET and HEAD) at a time and sends responses using TCP

client.py and server.py implement an Authoritative DNS server using a Client-Server
system that uses UDP socket. 


### - How to run webserver.py
To run this webserver simply run this in your terminal

```bash
  python webserver.py
```
The output is the terminal should be
"The server http://127.0.0.1:13000 is ready to receive"
GET and HEAD requests can be tested by using postman
GET requests can also be tested by opening http://127.0.0.1:13000/{resource} in your web broser

Resources that can be requested with the current file structure include:
page.html
Sub\HyperText.html

To end the webserver close the terminal

### - How to run client.py and server.py 
To send DNS QUERYS from a client.py and recvive a response from a DNS server

Open two terminals 
In the first run
```bash
  python server.py
```
You should see this in that terminal 
"The server is ready to receive"

In the second terminal run
```bash
  python client.py
```
Respond to the "Enter Domain Name: " prompt with the domain you want to search for or "end" to end the session 
### Note: Do not inclue the 'www.' before the domain name, this will return Domain not found

In the server terminal your sshould see the the DNS Query and the Response to the query in hex
In the client terminal the result of the query will be returned with the format 

example.com : type A, class IN, TTL 160, addr 4 192.60.28.4
or
Domain example/com not found!

To end the server close the terminal

## Authors: 
### Group 15
- Albert Ndur-Osei
- Emilia Zerbe
