from socket import *
import struct
import random

##### HEADER FIELDS REMAINING CONSTANT FOR THE PURPOSES OF THIS LAB AS SPECIFIED IN THE MANUAL
message = 0 # future packed message
HEADER_ID = 0 # Random generated 16 bits
### FLAGS SECTION: ###
#QR: (1 bit) if query 0, if response 1
#OPCODE: 0 (4 bits)
#AA: (1 bit) manual says set to 1 for lab, but piazza TA says more correct for 0 for querys and 1 for responses
#TC: 0 (1 bit)
#RD: 0 (1 bit)
#RA: 0 (1 bit)
#Z: 0 (3 bits)
#RCODE: 0 (4 bits)
# done with flags section
HEADER_QDCOUNT = 1 # unsigned 16 bit integer - set to 1
HEADER_ANCOUNT = 0 # unsigned 16 bit integer -- default 0 for query (response might differ - explained later)
HEADER_NSCOUNT = 0 # unsigned 16 bit integer - set to 0
HEADER_ARCOUNT = 0 # unsigned 16 bit integer - set to 0

##### QUESTION SECTION FIELDS CONSTANT FOR THE PURPOSES OF THIS LAB AS SPECIFIED IN THE MANUAL
QTYPE = 1  # 2 octets (16 bits) and for TYPE A, the value is 1 (00 01 in hex)
QCLASS = 1 # 2 octets (16 bits) and for class IN, the value is 1 (00 01 in hex) for the internet

##### ANSWER SECTION FIELDS CONSTANT FOR THE PURPOSES OF THIS LAB AS SPECIFIED IN THE MANUAL
ANS_NAME = 49164 # will always be c0 0c in hex.
ANS_TYPE = 1 # hex 00 01 once packed
ANS_CLASS = 1
#ANS_TTL: 32 bit UNsigned int set based on domain table (260: 00 00 01 04 in hex; 160: 00 00 00 a0 in hex)
#ANS_RDLENGTH: 1 bit uint
#ANS_RDATA: variable length string of octets (IP addresses!!


serverIP = '127.0.0.1'
serverPort = 13000
clientSocket = socket(AF_INET, SOCK_DGRAM)
clientInput = input("Enter Domain Name: ")

while clientInput != "end":
    # header production - assemble and server side needs to modify QR, AA (both in FLAGS), ANCOUNT - lab manual AND TA IN PIAZZA
    HEADER_ID = random.getrandbits(16) #16 bit random sequence, shouldn't change btwn here and response
    # ACCORDING TO TA ON PIAZZA: AA bit for the response would be 1 as best practice, so here in the query its 0.
    FLAGS = 1024 # Only changing on the response side! (QR changing btwn client and server, so int value is 1024 or 33792)
    HEADER_ANCOUNT = 0 # 0 for query, 1 or more for the response
    
    # Creating DNS Header
    message = struct.pack('>HHHHHH',HEADER_ID,FLAGS,HEADER_QDCOUNT,HEADER_ANCOUNT,HEADER_NSCOUNT,HEADER_ARCOUNT)

    # producing the qname field for the query
    query_qname = ""
    # For each section of the user input separated by a period
    for inp in clientInput.lower().split('.'):
        # Append the ASCII character with the code length len(input)
        query_qname += chr(len(inp))
        query_qname += inp
    # at the Termination of the loop add 0 octet padding
    query_qname += chr(0)

    # Adding on the Query Section to the DNS Header
    message += query_qname.encode()
    message += struct.pack('>HH',QTYPE,QCLASS)
    
    # Attaching the server name and IP address to the message - then send into the socket!
    clientSocket.sendto(message, (serverIP, serverPort))
    
    # Wait for response from server 
    serverAddress = 0
    response = 0
    while serverAddress == 0:
        response, serverAddress = clientSocket.recvfrom(2048)
    
    # Parse response - want a loop for multiple records
    packedHeader = response[:12] # Header length is always going to be 12 octets
    HEADER_ID,FLAGS,HEADER_QDCOUNT,HEADER_ANCOUNT,HEADER_NSCOUNT,HEADER_ARCOUNT = struct.unpack('>HHHHHH', packedHeader)
    if(HEADER_ANCOUNT == 0 ): # no records
        print(f"Domain {clientInput.lower()} has no records found!")
    else:
        # for printing response, want to go more if multiple IP records
        qlen = len(message)
        for i in range(0,HEADER_ANCOUNT, 1):
            packedResult = response[qlen:qlen+12] # starting from response section
            qlen += 12
            ANS_NAME, ANS_TYPE, ANS_CLASS, ANS_TTL, ANS_RDLENGTH = struct.unpack('>HHHIH',packedResult)
            b0, b1, b2, b3 = struct.unpack('>BBBB', response[qlen:qlen+4])
            returnedIP_string = str(b0) + "." + str(b1) + "." + str(b2) + "." + str(b3)
            # insert domain, type and class constant, TTL, count IP length (lab always 4), and IP address decoded with period separators
            print(f"{clientInput.lower()}: type A, class IN, TTL {ANS_TTL}, addr ({ANS_RDLENGTH}) {returnedIP_string}")
            qlen += 4 # if another existing record, this increments the offset of the response start point
    
    # Next Domain Name
    clientInput = input("Enter Domain Name: ")
print("Session ended")

clientSocket.close()