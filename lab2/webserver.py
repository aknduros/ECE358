from socket import *
import os
from datetime import datetime


#Get/Head handler
def reqHandler(client):
    reqmsg = client.recv(2048).decode()
    #print(reqmsg)
    reqLines = reqmsg.split("\r\n")

    #Current time for response 
    current_datetime = datetime.now()
    http_date = current_datetime.strftime('%a, %d %b %Y %H:%M:%S GMT')
    server_name = "MyWebServer"
    try:
        if reqLines:
            method, path, _ = reqLines[0].split()
    except ValueError:
        return
    #Handle HTTP Methods (GET and HEAD)
    if method == "GET":
        file_path = path[1:]
        try:
            # Open the file for reading
            with open(file_path, "r", encoding="utf-8") as file:
                html_content = file.read()
            # Remove or replace newline characters
            html_content = html_content.replace("\n", "").replace("\r", "")
            # Now you can work with the HTML content as text
            modification_time = os.path.getmtime(file_path)
            # The modification_time is a timestamp in seconds since the epoch
            # You can convert the timestamp to a human-readable date and time
            modified = datetime.fromtimestamp(modification_time)
            modified_datetime = modified.strftime('%a, %d %b %Y %H:%M:%S GMT')
            #resource found 202 response 
            response = f"HTTP/1.1 200 OK\r\n"
            response += f"Date: {http_date}\r\n"
            response += f"Server: {server_name}\r\n"
            response += f"Last-Modified: {modified_datetime}\r\n"
            response += f"Content-Type: text/html\r\n"
            response += f"Content-Length: {len(html_content)}\r\n"
            response += f"Connection: Closed\r\n\r\n"
            response += f"{html_content}"
            
            client.send(response.encode())
            client.close()
            return
        #404 Not found error response 
        except FileNotFoundError: 
            response = f"HTTP/1.1 404 Not Found\r\n"
            response += f"Date: {http_date}\r\n"
            response += f"Server: {server_name}\r\n"
            response += f"Content-Type: text/html\r\n"
            response += f"Content-Length: 170\r\n"
            response += f"Connection: Closed\r\n\r\n"
            response += f"<!DOCTYPE html><html><head><title>404 Not Found</title></head><body><h1>404 - Not Found</h1><p>The requested resource could not be found on this server.</p></body></html>"
            client.send(response.encode())
            client.close()
            return
    elif method == "HEAD":
        file_path = path[1:]
        try:
            # Open the file for reading
            with open(file_path, "r", encoding="utf-8") as file:
                html_content = file.read()
            # Remove or replace newline characters
            html_content = html_content.replace("\n", "").replace("\r", "")
            # Now you can work with the HTML content as text
            modification_time = os.path.getmtime(file_path)
            # The modification_time is a timestamp in seconds since the epoch
            # You can convert the timestamp to a human-readable date and time
            modified = datetime.fromtimestamp(modification_time)
            modified_datetime = modified.strftime('%a, %d %b %Y %H:%M:%S GMT')
           
            #resource found 202 response 
            response = f"HTTP/1.1 200 OK\r\n"
            response += f"Date: {http_date}\r\n"
            response += f"Server: {server_name}\r\n"
            response += f"Last-Modified: {modified_datetime}\r\n"
            response += f"Content-Type: text/html\r\n"
            response += f"Content-Length: {len(html_content)}\r\n"
            response += f"Connection: Closed\r\n\r\n"
            
            client.send(response.encode())
            client.close()
            return
        #404 Not found error response 
        except FileNotFoundError: 
            response = f"HTTP/1.1 404 Not Found\r\n"
            response += f"Date: {http_date}\r\n"
            response += f"Server: {server_name}\r\n"
            response += f"Content-Type: text/html\r\n"
            response += f"Content-Length: 170\r\n"
            response += f"Connection: Closed\r\n\r\n"
            
            client.send(response.encode())
            client.close()
            return
    else:
        response = "HTTP/1.1 501 Not Implemented\r\n\r\nMethod not supported"
        client.send(response.encode())
        client.close()

    
        

if __name__ == "__main__":
    serverIP="127.0.0.1"
    serverPort = 13000
    serverSocket = socket(AF_INET,SOCK_STREAM)
    serverSocket.bind((serverIP, serverPort))
    serverSocket.listen(1)
    print ("The server http://127.0.0.1:13000 is ready to receive")
    
    while True:
        connectionSocket, addr = serverSocket.accept()
        reqHandler(connectionSocket)
pass
